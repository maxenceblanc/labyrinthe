#! /usr/bin/env python
#-*- coding: utf-8 -*-

########################
# Python 3.7
# Author : Maxence Blanc - https://github.com/maxenceblanc
########################

# IMPORTS
import time
import pprint
import random as rd

# CUSTOM IMPORTS
import utility


''' TO DO LIST
'''


''' NOTES
'''

####################################################
###################| CLASSES |######################
####################################################

class Maze():
    """ Object representation of a maze.
    """

    def __init__(self):
        
        self.matrix = []

        # Coordinates of the traveller
        self.traveller_x = None
        self.traveller_y = None



    ### MAZE CREATION ###

    def create(self, width: int, length: int, perfect=True, amount_to_break=None):
        """ Handles the creation of a maze, based on dimensions.

        INPUTS:
                dimensions of the maze (walls size ignored)
                option: create a perfect maze
                
        """
        
        self.matrix = Maze.baseFrame(width, length)

        destructibles = Maze.destructiblesList(self.matrix)

        if perfect:
            Maze.breakWalls(self.matrix, destructibles)

        else:
            Maze.breakWalls(self.matrix, destructibles, perfect=False, amount=amount_to_break)
            

    def baseFrame(width: int, length: int):
        """
        Generates a matrix of -1 surrounding values from 0 to (x * y - 1).
        It represents the base frame for the creation of a perfect maze.
        
        INPUTS: 
                length of the matrix
                width of the matrix

        OUTPUT: 
                the matrix
        """

        # Generating a matrix using input dimensions and initialising the acc
        matrix, acc = [[-1]*(2*width+1) for n in range(2*length+1)], 0

        # Iterating on y axis, step 2
        for i in range(1, len(matrix)-1, 2):
            # Iterating on x axis, step 2
            for j in range(1, len(matrix[0])-1, 2):

                # Slot takes the value of the acc
                matrix[i][j] = acc

                # Incrementing acc
                acc += 1

        return(matrix)


    def destructiblesList(matrix):
        """
        Returns the list of all destructible walls.

        INPUTS: 
                the matrix

        OUTPUT: 
                list of destructible walls coordinates
        """

        liste = []

        for x in range(1, len(matrix[0])-1):

            if x%2 == 1: 
                liste += [(x,y) for y in range(2, len(matrix)-1, 2)]

            else: 
                liste += [(x,y) for y in range(1, len(matrix)-1, 2)]


        return(liste)


    def breakWall(matrix, x, y, perfect=True):
        """
        Cherche la valeur_plus et la valeur_moins autour du mur selectionné.

        Entrée : la matrix
                les coordonnées du mur à détruire

        Retourne : 1 ou 0 qui indique au compteur du programme si un mur a été détruit ou non.
        """

        # en fonction de la colonne on sait si les valeurs à récupérer sont à gauche/droite ou en haut/bas
        if x%2 == 0: # gauche/droite
            valeur_plus, valeur_moins = max(matrix[y][x-1], matrix[y][x+1]), min(matrix[y][x-1], matrix[y][x+1])

        else: # haut/bas
            valeur_plus, valeur_moins = max(matrix[y-1][x], matrix[y+1][x]), min(matrix[y-1][x], matrix[y+1][x])

        # Changement des valeurs si les deux valeurs trouvées sont differentes, pour obtenir un labyrinthe parfait.
        if valeur_moins != valeur_plus or not perfect:
            matrix[y][x] = valeur_moins # on casse le mur

            # On propage la valeur_moins par récursivité
            Maze.propagation(matrix, x, y, valeur_moins)
            return(1)

        return(0)


    def propagation(matrix, x, y, valeur_moins):
        """
        Prend un point ou la valeur moins a déja été appliquée et recherche autours les valeurs à changer.
        
        Entrée : la matrix
                les coordonnées d'un point deja modifié
                la valeur à propager

        Retourne : rien (puisque la matrix se modifie)
        """

        if matrix[y][x+1] > valeur_moins: # à droite
            # Applique la nouvelle valeur
            matrix[y][x+1] = valeur_moins
            # Propage depuis ce point
            Maze.propagation(matrix, x + 1, y, valeur_moins)

        if matrix[y][x-1] > valeur_moins: # à gauche
            # Applique la nouvelle valeur
            matrix[y][x-1] = valeur_moins
            # Propage depuis ce point
            Maze.propagation(matrix, x - 1, y, valeur_moins)


        if matrix[y+1][x] > valeur_moins: # en bas
            # Applique la nouvelle valeur
            matrix[y+1][x] = valeur_moins
            # Propage depuis ce point
            Maze.propagation(matrix, x, y + 1, valeur_moins)

        if matrix[y-1][x] > valeur_moins: # en haut
            # Applique la nouvelle valeur
            matrix[y-1][x] = valeur_moins
            # Propage depuis ce point
            Maze.propagation(matrix, x, y - 1, valeur_moins)


    def breakWalls(matrix, destructibles_list, perfect=True, amount=None):
        """ Handles the wall destruction during the creation of the maze.
        """

        length = (len(matrix)-1)/2 
        width  = (len(matrix[0])-1)/2 

        if perfect:
            amount = (width * length -1) # sachant qu'on va en detruire x*y-1

        compteur = 0
        while compteur < amount and len(destructibles_list)>0:

            target = rd.randint(0, len(destructibles_list)-1) # On choisit un mur

            # Détruit le mur
            compteur += Maze.breakWall(matrix, destructibles_list[target][0], destructibles_list[target][1], perfect=perfect)
            destructibles_list.pop(target) # On retire le mur deja tester de la liste



    ### MAZE SOLVING ###

    # def solve(self):
    #     """
    #     """

    def wallFollower(self, orientation: int, hand="r"):
        """ Wall follower implementation.
        """





    def pledge(self, orientation: int, rotations_sum: int):
        """

        INPUTS:
                orientation: degrees relative to north devided by 90 
                    so 0 for north, 1 for east, etc.
                sum of the rotations made (+90's and -90's)
        """

        if orientation == 0:
            if self.matrix[self.traveller_y-1][self.traveller_x] == 0:
                self.traveller_y-=1
            else:
                orientation = (orientation + 1)%4
                rotations_sum += 1


        elif orientation == 1:
            if self.matrix[self.traveller_y][self.traveller_x+1] == 0:
                self.traveller_x+=1
            else:
                orientation = (orientation + 1)%4
                rotations_sum += 1


        elif orientation == 2:
            if self.matrix[self.traveller_y+1][self.traveller_x] == 0:
                self.traveller_y+=1
            else:
                orientation = (orientation + 1)%4
                rotations_sum += 1


        elif orientation == 3:
            if self.matrix[self.traveller_x-1][self.traveller_x] == 0:
                self.traveller_x-=1
            else:
                orientation = (orientation + 1)%4
                rotations_sum += 1

        









####################################################
##################| FUNCTIONS |#####################
####################################################



####################################################
###################| CONSTANTS |####################
####################################################


####################################################
####################| PROGRAM |#####################
####################################################

if __name__ == '__main__':

    maze = Maze()

    maze.create(5, 5, True)

    utility.dessin(maze.matrix)


    # Adding traveller

    maze.matrix[1][1] = 1
    maze.traveller_x = 1
    maze.traveller_y = 1

    utility.dessin(maze.matrix)

    maze.pledge(0, 0)


    # start_time = time.time()

    # for i in range(1000):
    #     Maze.listeCloisons(mat)

    # print("--- %s seconds ---" % (time.time() - start_time))
