#! /usr/bin/env python
#-*- coding: utf-8 -*-

########################
# Python 3.7
# Author : Maxence Blanc - https://github.com/maxenceblanc
########################

# IMPORTS

# CUSTOM IMPORTS


''' TO DO LIST
'''


''' NOTES
'''

####################################################
###################| CLASSES |######################
####################################################


####################################################
##################| FUNCTIONS |#####################
####################################################


def verifSaisie(question):
    """
    Demande une valeur à l'utilisateur et verifie qu'il s'agit bien d'un entier.
    Entrée : une question pour l'utilisateur
    Retourne : le nombre entré
    """

    entree = -1

    # Demande à l'utilisateur une entree jusqu'a ce que ce soit un entier.
    while entree < 0:
        try: # Verifie si la valeur entrée est de type entier.
            entree = int(input(question))
        except ValueError: # Dans le cas contraire, on affiche une erreur.
            print("Entrée invalide.")
    return entree


### Affichage ### ----------------------------------


def dessin(matrice):
    """
    Affiche ligne par ligne le labyrinthe en fonction des valeurs de la matrice
    Entrée : la matrice
    Retourne : Rien (procédure)
    """
    
    for y in range(len(matrice)):
        ligne = "" # Reinitialise la ligne
        for x in range(len(matrice[0])):
            # chaque caractere correspond a une valeur de la matrice
            # Les murs
            if matrice[y][x] == -1 :
                ligne += ('\x1b[0;37;47m' + '  ' + '\x1b[0m')
            # Les espaces libres
            elif matrice[y][x] == 0 :
                ligne += "  "
            # L'entrée
            elif matrice[y][x] == -2 :
                ligne += "A "
            # La sortie
            elif matrice[y][x] == -3 :
                ligne += "B "
            # Chemin le plus court
            elif matrice[y][x] > 0 or matrice[y][x] == -4 : # Chemin le plus court
                ligne += ('\x1b[0;33;43m' + '  ' + '\x1b[0m')
        print(ligne)


####################################################
###################| CONSTANTS |####################
####################################################


####################################################
####################| PROGRAM |#####################
####################################################

if __name__ == '__main__':

    pass